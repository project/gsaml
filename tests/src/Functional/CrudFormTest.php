<?php

namespace Drupal\Tests\gsaml\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group gsaml
 */
class CrudFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'gsaml',
    'group',
    'groupmedia',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'seven';

  /**
   * Test access to site.
   */
  public function testConfigurationForm() {
    // Going to the config page.
    $this->drupalGet('/admin/group/saml');

    // Checking that the page is not accesible for anonymous users.
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->drupalCreateUser([
      'administer group',
    ]);

    $this->drupalLogin($account);

    // Going to the config page.
    $this->drupalGet('/admin/group/saml');

    // Checking the page title.
    $this->assertSession()->elementTextContains('css', 'h1', 'Group SAML settings');

  }

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'administer group',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/group/saml');
    $this->assertSession()->pageTextContains($this->t('Group SAML settings'));
  }

  /**
   * Test submit to configuration page.
   */
  public function testSubmitConfigPage() {
    $account = $this->drupalCreateUser([
      'administer group',
      'administer taxonomy',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/group/saml');
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save configuration';
    $this->assertSession()->buttonExists($submit_button);
  }

}
