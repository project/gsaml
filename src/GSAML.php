<?php

namespace Drupal\gsaml;

use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupType;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * GSAML helper class.
 */
class GSAML {

  /**
   * Batch execution.
   */
  public static function associateContentToGroup($type, $input, &$context) {
    $grelations = $input['grelations'];
    $process_n_nodes = $input["process_n_$type"];
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    // In batch: For each content associated to that term(s), associate content
    // to corresponding group(s).
    if ($process_n_nodes && is_numeric($process_n_nodes)) {
      // Initiate multistep processing.
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $process_n_nodes;
        $context['sandbox']['curr_node'] = 0;
      }

      // Process the next 100 if there are at least 100 left. Otherwise,
      // we process the remaining number.
      $batch_size = 1;
      $max = $context['sandbox']['progress'] + $batch_size;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }
      // Start where we left off last time.
      $start = $context['sandbox']['progress'];
      for ($i = $start; $i < $max; $i++) {
        // Update our progress!
        $context['sandbox']['progress']++;
        // Get next node without group.
        $next = self::getNextIdWithTid($type, $context['sandbox']['curr_node']);
        if (empty($next)) {
          $context['sandbox']['progress'] = $context['sandbox']['max'];
          $context['finished'] = 1;
          break;
        }
        $id = array_keys($next);
        $id = (int) reset($id);
        $context['sandbox']['curr_node'] = $id;
        $tids = reset($next);
        if (empty($tids)) {
          continue;
        }
        foreach (explode(',', $tids) as $tid) {
          $gid = $config->get('mapping_terms.' . $tid);
          if (!is_numeric($gid)) {
            continue;
          }

          $group = Group::load($gid);
          if (empty($group)) {
            continue;
          }

          $entity = \Drupal::entityTypeManager()->getStorage($type)->load($id);
          if (empty($entity)) {
            continue;
          }
          $bundle = $entity->bundle();
          if (!in_array($bundle, $grelations)) {
            continue;
          }
          $gnode = $group->getContentByEntityId("group_$type:" . $bundle, $id);
          if (!empty($gnode)) {
            continue;
          }
          $group->addContent($entity, 'group_' . $entity->getEntityTypeId() . ':' . $bundle);
        }
      }

      // Multistep processing : report progress.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Count number of nodes.
   */
  public static function countEntity($entity) {
    $query = \Drupal::database()->select("$entity", 'n');
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get the next node larger than nid.
   */
  public static function getNextIdWithTid($type, $entity_id) {
    if (is_numeric($entity_id)) {
      $config_factory = \Drupal::configFactory();
      $config = $config_factory->getEditable('gsaml.settings');
      $field_names = $config->get("entity_id_$type");
      $id = $type == 'node' ? 'nid' : 'mid';
      $result = \Drupal::entityQuery("$type")
        ->accessCheck(TRUE)
        ->condition("$id", $entity_id, '>')
        ->sort("$id", 'ASC')
        ->execute();
      $next = reset($result);
      $entity = \Drupal::entityTypeManager()->getStorage($type)->load($next);
      $tids = [];
      foreach ($field_names as $field_name) {
        if (!is_null($entity) && !is_null($field_name) && $entity->hasfield($field_name)) {
          $values = $entity->get($field_name)->getValue();
          foreach ($values as $value) {
            if (!is_null($value['target_id'])) {
              $tids[$value['target_id']] = $value['target_id'];
            }
          }
        }
      }
    }
    return [$next => implode(',', $tids)];
  }

  /**
   * Associate Users to Group Roles and Roles.
   */
  public static function associateUsersToGroupRoles($input, &$context) {
    $process_n_users = $input['process_n_users'];
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    $field_name = $config->get('user_field');
    // In batch: For each user ADFS mapping, associate user to the corresponding
    // group(s), with the corresponding group role(s).
    if ($process_n_users && is_numeric($process_n_users) && !empty($field_name)) {
      // Initiate multistep processing.
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $process_n_users;
        $context['sandbox']['curr_user'] = 0;
      }

      // Process the next 1 if there are at least 1 left. Otherwise,
      // we process the remaining number.
      $batch_size = 1;
      $max = $context['sandbox']['progress'] + $batch_size;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }

      // Start where we left off last time.
      $start = $context['sandbox']['progress'];
      for ($i = $start; $i < $max; $i++) {
        // Update our progress!
        $context['sandbox']['progress']++;
        // Get next node without group.
        $uid = self::getNextUid($context['sandbox']['curr_user']);
        if (empty($uid) || $uid == 0) {
          $context['sandbox']['progress'] = $context['sandbox']['max'];
          $context['finished'] = 1;
          break;
        }
        $context['sandbox']['curr_user'] = $uid;
        $user = User::load($uid);
        if (empty($user)) {
          continue;
        }
        // Get user FS in 'field_name'.
        $fss = $user->get($field_name)->value;
        if (empty($fss)) {
          continue;
        }
        $fss = explode(PHP_EOL, $fss);
        foreach ($fss as $fs) {
          $fs = trim($fs);
          // Get FS mapping.
          $maps = $config->get('mapping.' . $fs);
          if (!is_array($maps)) {
            continue;
          }
          foreach ($maps as $map) {
            // Get group by term.
            $gid = $config->get('mapping_terms.' . $map['term']);
            if (empty($gid)) {
              continue;
            }
            // Load group.
            $group = Group::load($gid);
            if (empty($group)) {
              continue;
            }
            $guser = GroupContent::loadByEntity($user);
            // Add user to group with role.
            $group_roles = [];
            if (!empty($map['group_role'])) {
              $group_roles = ['group_roles' => $map['group_role']];
            }
            $gcid = self::getUserGroupContentsInGroup($user->id(), $gid);
            if (empty($gcid)) {
              $group->addMember($user, $group_roles);
            }
            else {
              $guser = GroupContent::load($gcid);
              if (!empty($guser)) {
                $roles = $guser->get('group_roles')->getValue();
                $rids = [];
                foreach ($roles as $role) {
                  $rids[] = $role['target_id'];
                }
                if (!empty($map['group_role'])) {
                  $rids[] = $map['group_role'];
                }
                $guser->set('group_roles', $rids);
                $guser->save();
              }
            }
            // If role is defined, add role to user.
            if (empty($map['role'])) {
              continue;
            }
            $user->addRole($map['role']);
            $user->save();
          }
        }
      }

      // Multistep processing : report progress.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Count number of users.
   */
  public static function countUsers() {
    $query = \Drupal::database()->select('users', 'u');
    $query->condition('u.uid', 0, '<>');
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get next user with UID larger than.
   */
  public static function getNextUid($uid) {
    $query = \Drupal::database()->select('users', 'u');
    $query->addField('u', 'uid');
    $query->condition('u.uid', 0, '<>');
    $query->condition('u.uid', $uid, '>');
    $query->orderBy('u.uid', 'ASC');
    $query->range(0, 1);
    return $query->execute()->fetchField();
  }

  /**
   * Remove all members from groups.
   */
  public static function removeMemberships($input, &$context) {
    $process_n_users = $input['remove_gusers'];
    // In batch: Remove all members from all groups.
    if ($process_n_users && is_numeric($process_n_users)) {
      // Initiate multistep processing.
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $process_n_users;
      }

      // Process the next 1 if there are at least 1 left. Otherwise,
      // we process the remaining number.
      $batch_size = 1;
      $max = $context['sandbox']['progress'] + $batch_size;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }

      // Start where we left off last time.
      $start = $context['sandbox']['progress'];
      for ($i = $start; $i < $max; $i++) {
        // Update our progress!
        $context['sandbox']['progress']++;
        // Get next node without group.
        $gcid = self::getNextUserWithGroup();
        if (empty($gcid)) {
          $context['sandbox']['progress'] = $context['sandbox']['max'];
          $context['finished'] = 1;
          break;
        }
        $guser = GroupContent::load($gcid);
        if (empty($guser)) {
          continue;
        }
        $guser->delete();
      }

      // Multistep processing : report progress.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Count number of users that are in a group.
   */
  public static function countUsersWithGroup() {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->condition('g.type', 0, '<>');
    $query->condition('g.type', '%group_membership', 'LIKE');
    return $query->countQuery()->execute()->fetchField();

  }

  /**
   * Get the next user with a group.
   */
  public static function getNextUserWithGroup() {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->addField('g', 'id');
    $query->condition('g.type', 0, '<>');
    $query->condition('g.type', '%group_membership', 'LIKE');
    $query->orderBy('g.id', 'ASC');
    $query->range(0, 1);
    return $query->execute()->fetchField();
  }

  /**
   * Remove all roles from users.
   */
  public static function removeRoles($input, &$context) {
    $process_n_users = self::countUsersWithRole();
    // In batch: Remove all members from all groups.
    if ($process_n_users && is_numeric($process_n_users)) {
      // Initiate multistep processing.
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $process_n_users;
      }

      // Process the next 1 if there are at least 1 left. Otherwise,
      // we process the remaining number.
      $batch_size = 1;
      $max = $context['sandbox']['progress'] + $batch_size;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }

      // Start where we left off last time.
      $start = $context['sandbox']['progress'];
      for ($i = $start; $i < $max; $i++) {
        // Update our progress!
        $context['sandbox']['progress']++;
        // Get next node without group.
        $next = self::getNextUserWithRole();
        if (empty($next)) {
          $context['sandbox']['progress'] = $context['sandbox']['max'];
          $context['finished'] = 1;
          break;
        }
        $rid = array_keys($next);
        $rid = reset($rid);
        $uid = (int) reset($next);
        $user = User::load($uid);
        if (empty($user)) {
          continue;
        }
        $user->removeRole($rid);
        $user->save();
      }

      // Multistep processing : report progress.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Count number of users that have roles.
   */
  public static function countUsersWithRole() {
    $query = \Drupal::database()->select('user__roles', 'ur');
    $query->condition('ur.entity_id', 1, '<>');
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get the next user with a role.
   */
  public static function getNextUserWithRole() {
    $query = \Drupal::database()->select('user__roles', 'ur');
    $query->addField('ur', 'roles_target_id');
    $query->addField('ur', 'entity_id');
    $query->condition('ur.entity_id', 1, '<>');
    $query->orderBy('ur.entity_id', 'ASC');
    $query->range(0, 1);
    return $query->execute()->fetchAllKeyed(0, 1);
  }

  /**
   * Delete all group content.
   */
  public static function removeGroupContent($input, &$context) {
    $process_n_nodes = $input['remove_gnodes'];
    // In batch: For each content associated to that term(s), associate content
    // to corresponding group(s).
    if ($process_n_nodes && is_numeric($process_n_nodes)) {
      // Initiate multistep processing.
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $process_n_nodes;
      }

      // Process the next 100 if there are at least 100 left. Otherwise,
      // we process the remaining number.
      $batch_size = 1;
      $max = $context['sandbox']['progress'] + $batch_size;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }

      // Start where we left off last time.
      $start = $context['sandbox']['progress'];
      for ($i = $start; $i < $max; $i++) {
        // Update our progress!
        $context['sandbox']['progress']++;
        // Get next node without group.
        $gcid = self::getNextGroupNode();
        if (empty($gcid)) {
          $context['sandbox']['progress'] = $context['sandbox']['max'];
          $context['finished'] = 1;
          break;
        }
        $gnode = GroupContent::load($gcid);
        if (empty($gnode)) {
          continue;
        }
        $gnode->delete();
      }

      // Multistep processing : report progress.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Count number group contents.
   */
  public static function countGroupNodes() {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->condition('g.type', '%group_membership', 'NOT LIKE');
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get the next group content.
   */
  public static function getNextGroupNode() {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->addField('g', 'id');
    $query->condition('g.type', '%group_membership', 'NOT LIKE');
    $query->orderBy('g.entity_id', 'ASC');
    $query->range(0, 1);
    return $query->execute()->fetchField();
  }

  /**
   * Batch execution.
   */
  public static function associateSingleContentToGroup($entity, $terms) {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    foreach ($terms as $term) {

      $tid = $term;
      $gid = $config->get('mapping_terms.' . $tid);
      if (empty($gid)) {
        continue;
      }

      $group = Group::load($gid);
      if (empty($group)) {
        continue;
      }

      $group->addContent($entity, 'group_' . $entity->getEntityTypeId() . ':' . $entity->bundle());

    }
  }

  /**
   * Get the user membership.
   */
  public static function getUserGroupContents($uid) {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->condition('g.type', '%group_membership', 'LIKE');
    $query->condition('g.entity_id', $uid);
    $query->addField('g', 'id');
    $result = $query->execute();
    $next = $result->fetchAllKeyed(0, 0);
    return is_array($next) ? $next : [$next];
  }

  /**
   * Get the next user with a group.
   */
  public static function getUserGroupContentsInGroup($uid, $gid) {
    $query = \Drupal::database()->select('group_content_field_data', 'g');
    $query->condition('g.type', '%group_membership', 'LIKE');
    $query->condition('g.entity_id', $uid);
    $query->condition('g.gid', $gid);
    $query->addField('g', 'id');
    $result = $query->execute();
    $next = $result->fetchAllKeyed(0, 0);
    return is_array($next) ? (int) reset($next) : $next;
  }

  /**
   * Get the next user with a group.
   */
  public static function saveEntity($input, &$context) {
    $n_nodes = $input['n_nodes'];
    $type = $input['type'];
    if ($n_nodes && is_numeric($n_nodes)) {
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $n_nodes;
        $context['sandbox']['curr_node'] = 0;
      }
      $max = $context['sandbox']['progress'] + 1;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }
      $nid = self::getNextEntityWithoutGroupLargerThan($context['sandbox']['curr_node'], $type);
      $context['sandbox']['curr_node'] = $nid;
      if ($type == 'media') {
        $entity = Media::load($nid);
      }
      else {
        $entity = Node::load($nid);
      }
      self::entityUpdate($entity);
      $context['sandbox']['progress']++;
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Get next entity from group.
   */
  public static function getNextEntityWithoutGroupLargerThan($nid, $type) {
    if (!is_numeric($nid)) {
      return;
    }
    $key = $type != 'media' ? 'nid' : 'mid';
    $query = \Drupal::database()->select($type, 'n');
    $query->leftJoin('group_content_field_data', 'g', "g.entity_id = n.$key");
    $query->condition($key, $nid, '>');
    $query->isNull('g.id');
    $query->addField('n', $key);
    $query->orderBy($key, 'ASC');
    $query->range(0, 1);
    $next = $query->execute()->fetchField();
    return is_array($next) ? (int) reset($next) : (int) $next;
  }

  /**
   * Entity update.
   */
  public static function entityUpdate($entity) {
    // If gsaml.settings.yml is defined.
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');

    if (empty($config)) {
      return;
    }

    // Get Installed Plugins in Group Type defined in gsaml.settings.yml.
    $group_type = $config->get('group_type');
    if (empty($group_type)) {
      return;
    }

    $grelations = [];
    foreach (GroupType::load($group_type)->getInstalledContentPlugins() as $grelation) {
      $plugin_id = $grelation->getConfiguration()['id'];
      $gentity = strpos($plugin_id, ':') !== FALSE ? explode(':', $plugin_id)[1] : '';
      if (empty($gentity)) {
        continue;
      }
      $grelations[] = $gentity;
    }

    // Check if node exists in Installed Plugins.
    $bundle = $entity->bundle();

    if (!in_array($bundle, $grelations)) {
      return;
    }

    // If this node is in Installed Plugins, delete all group contents linked
    // to this node.
    $gentities = GroupContent::loadByEntity($entity);

    foreach ($gentities as $gentity) {
      $gentity->delete();
    }

    // Get all groups associated to this node terms (taxonomy).
    $terms = self::getEntityTerms($entity);

    self::associateSingleContentToGroup($entity, $terms);
  }

  /**
   * Get entity terms.
   */
  public static function getEntityTerms($entity) {
    $id = $entity->id();
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');

    if (empty($config)) {
      return;
    }
    $e_fields = $config->get('entity_id_' . $entity->getEntityTypeId());

    $next = [];
    if (is_numeric($id)) {
      foreach ($e_fields as $e_field) {
        $e_field_id = $e_field . '_target_id';
        $e_field_table = $entity->getEntityTypeId() . '__' . $e_field;
        $query = \Drupal::database()->select($e_field_table, 'ti');
        $query->condition('ti.entity_id', $id);
        $query->addField('ti', $e_field_id);
        $result = $query->execute();
        $keys = $result->fetchAllKeyed(0, 0);
        foreach ($keys as $key) {
          $next[$key] = $key;
        }
      }
    }
    return array_keys($next);
  }

  /**
   * Update groups with terms.
   */
  public static function updateTax($input, &$context) {
    $n_terms = $input['n_terms'];
    $vid = $input['vid'];
    $group_field = $input['group_field'];
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    if ($n_terms && is_numeric($n_terms) && !empty($vid)) {
      if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $n_terms;
        $context['sandbox']['curr_term'] = 0;
      }
      $max = $context['sandbox']['progress'] + 1;
      if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
      }
      $tid = self::getNextTermLargerThan($context['sandbox']['curr_term'], $vid);
      // Get group from mapping.
      $gid = $config->get('mapping_terms.' . $tid);
      if (!empty($gid)) {
        $term = Term::load($tid);
        $group = Group::load($gid);
        if (empty($group)) {
          $aux = $config->get('mapping_terms');
          $new_group = [];
          $new_group['langcode'] = 'en';
          $new_group['label'] = $term->label();
          $new_group['type'] = $config->get('group_type');
          $new_group['uid'] = 1;
          $group = Group::create($new_group);
          $group->save();
        }
        $group->set($group_field, $term);
        $group->save();
        $config->set('mapping_terms.' . $tid, $group->id());
        $config->save();
      }
      $context['sandbox']['curr_term'] = $tid;
      $context['sandbox']['progress']++;
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Check if any mapping term was deleted and update groups.
   */
  public static function checkMappingTerms($input, &$context) {
    $vid = $input['vid'];

    $query = \Drupal::database()->select('taxonomy_term_data', 't');
    $query->addField('t', 'tid');
    $query->condition('t.vid', $vid);
    $query->orderBy('t.tid', 'ASC');
    $terms = $query->execute()->fetchAllKeyed(0, 0);

    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    $keys = $config->get('mapping_terms');
    foreach ($keys as $key) {
      if (!array_key_exists($key, $terms)) {
        unset($keys[$key]);
        $config->set('mapping_terms', $keys)->save();
      }
    }
    $config_groups = array_values($keys);
    $groups = \Drupal::entityTypeManager()->getStorage('group')->loadMultiple();
    foreach ($groups as $group) {
      if (!is_null($group) && !in_array($group->id(), $config_groups)) {
        \Drupal::logger('gsaml_key_not_in')->info($group->id());
        $group->delete();
      }
    }

  }

  /**
   * Count terms with vid.
   */
  public static function countTermsInVocabulary($vid) {
    $query = \Drupal::database()->select('taxonomy_term_data');
    $query->condition('vid', $vid);
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get next term.
   */
  public static function getNextTermLargerThan($tid, $vid) {
    if (!is_numeric($tid)) {
      return;
    }
    $query = \Drupal::database()->select('taxonomy_term_data', 't');
    $query->addField('t', 'tid');
    $query->condition('t.vid', $vid);
    $query->condition('t.tid', $tid, '>');
    $query->orderBy('t.tid', 'ASC');
    $query->range(0, 1);
    $result = $query->execute();
    $next = $result->fetchAllKeyed(0, 0);
    return is_array($next) ? (int) reset($next) : (int) $next;
  }

  /**
   * Remove users from groups.
   */
  public static function removeUserMemberships($account) {
    $roles = $account->getRoles();
    foreach ($roles as $role) {
      $account->removeRole($role);
    }
    $account->save();

    $gcids = GSAML::getUserGroupContents($account->id());
    foreach ($gcids as $gcid) {
      $guser = GroupContent::load($gcid);
      if (empty($guser)) {
        continue;
      }
      $guser->delete();
    }
  }

  /**
   * Batch execution.
   */
  public static function associateSingleUserToGroup($user, $fss = NULL) {

    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('gsaml.settings');
    $field_name = $config->get('user_field');

    if ($fss === NULL) {
      $fss = $user->get($field_name)->value;

      if (!is_array($fss)) {
        $fss = explode(PHP_EOL, $fss);
      }
    }

    foreach ($fss as $fs) {
      $fs = trim($fs);
      $maps = $config->get('mapping.' . $fs);

      if (!is_array($maps)) {
        continue;
      }
      foreach ($maps as $map) {
        // Get group by term.
        $gid = $config->get('mapping_terms.' . $map['term']);

        if (!is_numeric($gid)) {
          continue;
        }

        // Load group.
        $group = Group::load($gid);

        if (empty($group)) {
          continue;
        }
        // Add user to group with role.
        $group_roles = [];
        if (!empty($map['group_role'])) {
          $group_roles = ['group_roles' => $map['group_role']];
        }
        $gcid = self::getUserGroupContentsInGroup($user->id(), $gid);

        if (empty($gcid)) {
          $group->addMember($user, $group_roles);
        }
        else {
          $guser = GroupContent::load($gcid);
          if (!empty($guser)) {
            $roles = $guser->get('group_roles')->getValue();
            $rids = [];
            foreach ($roles as $role) {
              $rids[] = $role['target_id'];
            }
            if (!empty($map['group_role'])) {
              $rids[] = $map['group_role'];
            }
            $guser->set('group_roles', $rids);
            $guser->save();
          }
        }
        // If role is defined, add role to user.
        if (!empty($map['role'])) {
          $user->addRole($map['role']);
          $user->save();
        }
      }
    }
  }

}
